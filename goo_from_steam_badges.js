/**
 * This was caught on November 21, 2014 and removed shortly after.
 * 
 * Source commit: https://github.com/SteamDatabase/SteamTracking/commit/c81b7e75eb52806c0854efa34abe70d5a7fdd006
 */

function GrindIntoGoo( appid, contextid, itemid )
{
	var rgAJAXParams = {
		sessionid: g_sessionID,
		appid: appid,
		assetid: itemid,
		contextid: contextid
	};
	var strActionURL = g_strProfileURL + "/ajaxgetgoovalue/";

	$J.get( strActionURL, rgAJAXParams ).done( function( data ) {
		var $Content = $J(data.strHTML);
		var strDialogTitle = data.strTitle;
		ShowConfirmDialog( strDialogTitle, $Content ).done( function() {
			strActionURL = g_strProfileURL + "/ajaxgrindintogoo/";
			rgAJAXParams.goo_value_expected = data.goo_value;

			$J.post( strActionURL, rgAJAXParams).done( function( data ) {
				ShowAlertDialog( strDialogTitle, data.strHTML );
				ReloadCommunityInventory();
			}).fail( function() {
				ShowAlertDialog( strDialogTitle, 'There was an error communicating with the network. Please try again later.' );
			});
		});
	});
}

function PackGameGooIntoBarrel( appid, itemid )
{
	var rgAJAXParams = {
		sessionid: g_sessionID,
		appid: appid,
		assetid: itemid,
		goo_denomination_in: 1,
		goo_amount_in: 1000,
		goo_denomination_out: 1000,
		goo_amount_out_expected: 1
	};
	var strActionURL = g_strProfileURL + "/ajaxexchangegoo/";

	$J.post( strActionURL, rgAJAXParams).done( function( data ) {
			if ( data.success == 78 )
			{
				ShowAlertDialog( 'Action Failed', 'You need at least 1000 Goo to make a Goo Barrel' );
			}
			else
			{
				ShowAlertDialog( 'Success', 'New Barrel Acquired' );		// localize
				ReloadCommunityInventory();
			}
		}).fail( function() {
			ShowAlertDialog( 'Action Failed', 'There was an error communicating with the network. Please try again later.' );
		});
}

function UnpackGameGooFromBarrel( appid, itemid )
{
	var rgAJAXParams = {
		sessionid: g_sessionID,
		appid: appid,
		assetid: itemid,
		goo_denomination_in: 1000,
		goo_amount_in: 1,
		goo_denomination_out: 1,
		goo_amount_out_expected: 1000
	};
	var strActionURL = g_strProfileURL + "/ajaxexchangegoo/";

	$J.post( strActionURL, rgAJAXParams).done( function( data ) {
			ShowAlertDialog( 'Success', '1000 Goo acquired from barrel' );		// localize
			ReloadCommunityInventory();
		}).fail( function() {
			ShowAlertDialog( 'Action Failed', 'There was an error communicating with the network. Please try again later.' );
		});
}
